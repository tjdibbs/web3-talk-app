import { Button, Card, Typography } from "antd";
import { NavLink } from "react-router-dom";

export default function ErrorPage() {
  return (
    <Card
      style={{
        marginTop: "2em",
        borderRadius: "20px",
        boxShadow: "var(--primary-color)",
      }}
    >
      <Typography.Title level={4}>Page no found</Typography.Title>
      <Typography.Text>The page you looking for does not exist</Typography.Text>
      <div>
        <NavLink to={"/"}>
          <Button
            style={{
              background: "var(--primary-color)",
              color: "#fff",
              marginTop: "1em",
            }}
          >
            Go back home
          </Button>
        </NavLink>
      </div>
    </Card>
  );
}
