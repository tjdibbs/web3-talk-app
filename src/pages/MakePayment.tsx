import React from "react";
import { useMoralis, useWeb3Transfer, useMoralisWeb3Api } from "react-moralis";
import NoProfile from "../components/noProfile";
import { useAppSelector } from "../hooks/store";
import NotConnected from "../components/notConnected";
import "../styles/payment.css";
import { Box, Card, Chip, Paper, Stack } from "@mui/material";
import Chains from "../components/header/Chains";
import { Input, Button, Dropdown, Menu, Typography } from "antd";
import { useForm } from "react-hook-form";
import { DownOutlined, UpOutlined } from "@ant-design/icons";
import { ETHLogo, USDTLogo } from "../components/header/Chains/Logos";
import Transactions from "src/components/Transactions";

const styles = {
  item: {
    display: "flex",
    alignItems: "center",
    height: "42px",
    gap: "1em",
    fontWeight: "500",
    fontFamily: "Roboto, sans-serif",
    fontSize: "14px",
    padding: "0 10px",
    width: "100%",
  },
  button: {
    borderRadius: "20px",
    backgroundColor: "rgba(215,136,63,0.25)",
    color: "#BB702C",
    display: "grid",
    placeItems: "center",
    boxShadow: "none",
    fontWeight: 600,
    border: "none",
    height: 50,
  },
  input: {
    padding: "1em",
    borderRadius: "20px",
    background: "#f2f2f2",
    boxShadow: "0 0 10px #D7883F25, inset 0 0 20px #D7883F25",
  },
};

type FormData = {
  currency: string;
  receiver: string;
  amount: number;
};

type Selected = Partial<{
  key: string;
  icon: JSX.Element;
  value: string;
}>;

const menuItems = [
  {
    type: "native",
    value: "Ethereum",
    icon: <ETHLogo />,
  },
  {
    type: "erc20",
    value: "USDT",
    icon: <USDTLogo />,
  },
];

function MakePayment() {
  const { isAuthenticated, account, Moralis } = useMoralis();
  const {
    register,
    watch,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm<FormData>({
    defaultValues: {
      amount: 0,
      receiver: "",
    },
  });
  const activeUser = useAppSelector((state) => state.web3Talk.user);
  const { receiver, amount } = watch();
  const [selected, setSelected] = React.useState<Selected>(menuItems[1]);

  if (!isAuthenticated || !account) {
    return (
      <div className="home">
        <NotConnected />
      </div>
    );
  }

  const onSubmit = async (data: FormData) => {
    const options: {
      amount: string;
      receiver: string;
      type: string;
      contractAddress?: string;
    } = {
      amount: Moralis.Units.Token(data.amount, 18),
      receiver: data.receiver,
      type: "native",
    };

    if (selected.value === "USDT") {
      (options.type = "erc20"),
        (options.contractAddress =
          "0xdAC17F958D2ee523a2206206994597C13D831ec7");
    }

    //@ts-ignore
    const hash = await Moralis.transfer(options);

    console.log({ hash });
  };

  const menu = (
    <Menu
      onClick={(s) => {
        setValue("currency", s.key);
        setSelected(menuItems.find((menu) => menu.type == s.key) as Selected);
      }}
    >
      {menuItems.map((item) => (
        <Menu.Item key={item.type} icon={item.icon} style={styles.item}>
          <span
            style={{
              marginLeft: "5px",
              fontWeight: 500,
              color: "rgba(0,0,0,.8)",
            }}
          >
            {item.value}
          </span>
        </Menu.Item>
      ))}
    </Menu>
  );

  return (
    <main className="main-content">
      <div className="navigation-flow">
        <a href="/home">
          <span>Home</span>
        </a>
        <span className="iconify" data-icon="ep:arrow-right"></span>
        <a href="#">
          <span style={{ color: "#ff7a00" }}>Payment</span>
        </a>
      </div>
      <Paper
        sx={{
          width: 400,
          maxWidth: "100%",
          borderRadius: "20px",
          mx: "auto",
          p: 2,
        }}
        onSubmit={handleSubmit(onSubmit)}
        component={"form"}
        autoComplete={"off"}
      >
        <Stack spacing={2}>
          <Box>
            <Dropdown overlay={menu} trigger={["click"]}>
              <Button
                key={selected?.key}
                icon={selected?.icon}
                style={{
                  ...styles.item,
                  ...styles.input,
                  height: 50,
                }}
              >
                <span
                  style={{
                    marginLeft: "5px",
                    flexGrow: 1,
                    fontWeight: 600,
                    color: "#BB702C",
                  }}
                >
                  {selected?.value}
                </span>
                <Stack spacing={0.5} alignItems={"center"}>
                  <UpOutlined style={{ fontSize: "10px", fill: "#BB702C" }} />
                  <DownOutlined style={{ fontSize: "10px", fill: "#BB702C" }} />
                </Stack>
              </Button>
            </Dropdown>
          </Box>
          <Box>
            <Input
              status={errors.receiver ? "error" : undefined}
              type={"text"}
              placeholder={"Receiver Address"}
              style={styles.input}
              {...register("receiver", {
                required: true,
                onChange: (e) => setValue("receiver", e.target.value),
              })}
            />
          </Box>
          <Box>
            <Input
              status={errors.amount ? "error" : undefined}
              type={"text"}
              placeholder={"Amount"}
              style={styles.input}
              {...register("amount", {
                required: true,
                onChange: (e) =>
                  setValue("amount", parseInt(e.target.value || "0")),
              })}
            />
          </Box>
          <Button
            type={"primary"}
            htmlType={"submit"}
            style={styles.button}
            size={"large"}
          >
            Transfer {selected.value}
          </Button>
        </Stack>
      </Paper>
      <Transactions />
    </main>
  );
}

export default MakePayment;
