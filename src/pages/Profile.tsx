import React from "react";
import { useMoralis } from "react-moralis";
import { UPDATEPROFILE, GETUSERPROFILE } from "../utils/constant";
import axios from "axios";
import { NavLink } from "react-router-dom";
import "../styles/profile.css";
import NotConnected from "../components/notConnected";
import { useAppSelector } from "../hooks/store";
import NoProfile from "../components/noProfile";
import { Avatar } from "antd";
import { ExtractSocialLinks } from "./edit-page";
import { User } from "../hooks/reducer";

function Profilepg() {
  const { isAuthenticated, user } = useMoralis();
  const activeUser = useAppSelector((state) => state.web3Talk.user);
  const socialLinks = ExtractSocialLinks(user?.toJSON() as unknown as User);

  type SocialKey = keyof typeof socialLinks;
  if (!isAuthenticated) {
    return (
      <div className="home">
        <NotConnected />
      </div>
    );
  }

  return (
    <main className="main-content">
      <div className="navigation-flow">
        <NavLink to="/home">
          <span>Home</span>
        </NavLink>
        <span className="iconify" data-icon="ep:arrow-right"></span>
        <a href="#">
          <span style={{ color: "#ff7a00" }}>Profile</span>
        </a>
      </div>
      <div className="profile-wrapper">
        <div className="user-details">
          <div className="user-image">
            <Avatar src={activeUser?.profile_image} size={100} />
          </div>
          <div className="user-name">
            <span style={{ textTransform: "capitalize" }}>
              {activeUser!.fullname ?? "no fullname"}
            </span>
            <div className="phone">
              <span className="iconify" data-icon="bi:telephone-fill"></span>
            </div>
          </div>
          <div className="user-position">
            <span>{activeUser!.role ?? "no position"}</span>
          </div>
          <div className="user-email">
            <span>{activeUser!.email ?? "no email"}</span>
          </div>
          <div className="social-wrapper">
            <div className="social-icons">
              {Object.keys(socialLinks).map((key) => (
                <React.Fragment key={key}>
                  {SOCIAL[key as SocialKey](
                    socialLinks[key as SocialKey] as string,
                  )}
                </React.Fragment>
              ))}
            </div>
          </div>
          <div className="sub-content">
            <div className="wallet">
              <h4>Wallet Address (USDT)</h4>
              <p>
                {activeUser?.usdtWalletAddress ?? (
                  <NavLink to="/profile/edit#wallet-address">
                    Add USDT Wallet Address
                  </NavLink>
                )}
              </p>
            </div>
          </div>
          <div className="edit-btn">
            <NavLink to="/profile/edit">
              <button className="edit btn action-btn">Edit Profile</button>
            </NavLink>
          </div>
        </div>
      </div>
      {/* End of profile details */}
    </main>
  );
}

const GitLab = (link: string) => (
  <a href={link}>
    <span className="iconify" data-icon="fa:gitlab"></span>
  </a>
);

const GitHub = (link: string) => (
  <a href={link}>
    <span className="iconify" data-icon="akar-icons:github-fill"></span>
  </a>
);

const Instagram = (link: string) => (
  <a href={link}>
    <span className="iconify" data-icon="ant-design:instagram-filled"></span>
  </a>
);

const Twitter = (link: string) => (
  <a href={link}>
    <span className="iconify" data-icon="akar-icons:twitter-fill"></span>
  </a>
);

const LinkedIn = (link: string) => (
  <a href={link}>
    <span className="iconify" data-icon="akar-icons:linkedin-box-fill"></span>
  </a>
);

const SOCIAL = {
  gitlab: GitLab,
  instagram: Instagram,
  twitter: Twitter,
  linkedin: LinkedIn,
  github: GitHub,
};

export default Profilepg;
