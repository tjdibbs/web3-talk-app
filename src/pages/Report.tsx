import React from "react";
import { useState } from "react";
import {
  useMoralis,
  useNewMoralisObject,
  useMoralisQuery,
} from "react-moralis";
import { SENDREPORTS, GETREPORTS } from "../utils/constant";
import axios from "axios";
import NotConnected from "../components/notConnected";
import "../styles/report.css";
import { useAppSelector } from "../hooks/store";
import NoProfile from "../components/noProfile";
import { useForm } from "react-hook-form";
import { Button, Collapse, Empty, Input } from "antd";
import { stringify } from "querystring";
import notification from "../components/notification";
import Moralis from "moralis/types";
import { User } from "../hooks/reducer";
import CollapsePanel from "antd/lib/collapse/CollapsePanel";
import { CaretRightOutlined } from "@ant-design/icons";

type Report = {
  walletAddress: string;
  topic: string;
  userid: string;
  detail: string;
  createdAt?: string;
  updatedAt?: string;
};

export default function Report() {
  const activeUser = useAppSelector((state) => state.web3Talk.user);
  const { isAuthenticated, user } = useMoralis();
  const Activities = useNewMoralisObject("Activities");
  const { fetch } = useMoralisQuery<Report>(
    "Activities",
    (query) => query.equalTo("userid", activeUser?.objectId as string),
    [],
    { autoFetch: false },
  );

  const {
    register,
    handleSubmit,
    watch,
    reset,
    formState: { errors },
    setValue,
  } = useForm<{ topic: string; detail: string }>();
  const { topic, detail } = watch();
  const [reportData, setReportData] = React.useState<Report[]>([]);

  const getReport = React.useCallback(async () => {
    try {
      const requestReports = await axios.get(GETREPORTS);
      const reports = (await requestReports.data).result;
      setReportData(reports);
    } catch (err: any) {
      alert(err.message);
    }
  }, []);

  React.useEffect(() => {
    if (isAuthenticated) {
      getReport();
    }
  }, [isAuthenticated, getReport]);

  const submitReport = async (report: { topic: string; detail: string }) => {
    try {
      const newReport: Report = {
        ...report,
        userid: activeUser?.objectId as string,
        walletAddress: user!?.get("ethAddress"),
      };

      // call save function to save report
      await Activities.save(newReport);

      // update reports
      setReportData([newReport, ...reportData]);

      // reset form fields
      reset();
    } catch (error: any) {
      console.error(error);
      notification(
        "error",
        "Error saving report",
        "Server Error or Internet Connection",
        "top",
      );
    }
  };

  React.useEffect(() => {
    (async () => {
      const reports = await fetch();
      let parsedReports: Report[] = [];

      reports?.forEach((report) => parsedReports.push(report.toJSON()));
      setReportData(parsedReports);
    })();
  }, []);

  if (!isAuthenticated) {
    return (
      <div className="home">
        <NotConnected />
      </div>
    );
  }

  // if (!activeUser?.email || !activeUser?.fullname) {
  //   return <NoProfile />;
  // }

  return (
    <main className="main-content">
      <div className="navigation-flow">
        <a href="/home">
          <span>Home</span>
        </a>
        <span className="iconify" data-icon="ep:arrow-right"></span>
        <a href="#">
          <span style={{ color: "#ff7a00" }}>Report</span>
        </a>
      </div>
      <div className="report-wrapper">
        <div className="report-label">
          <h3>Submit a Report</h3>
        </div>
        <form action="#" onSubmit={handleSubmit(submitReport)}>
          <div className="form-group">
            <Input
              type="text"
              placeholder="topic of the report..."
              className="form-control"
              value={topic}
              status={errors.topic && "error"}
              {...register("topic", {
                required: true,
                onChange: (e) => setValue("topic", e.target.value),
              })}
            />
          </div>
          <div className="form-group">
            <Input.TextArea
              placeholder="detail of the report...."
              className="form-control"
              id="detail"
              cols={30}
              rows={10}
              value={detail}
              status={errors.detail && "error"}
              {...register("detail", {
                required: true,
                onChange: (e) => setValue("detail", e.target.value),
              })}
            ></Input.TextArea>
          </div>
          <div className="action-group">
            <button className="btn" type="submit">
              Submit Report
            </button>
          </div>
        </form>
        <div className="submitted-report">
          <div className="topic">
            <h3>Reports Submitted</h3>
          </div>
          <div className="reports list" role="list">
            {reportData.length > 0 ? (
              listItems(reportData)
            ) : (
              <Empty
                image="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"
                imageStyle={{
                  height: 60,
                }}
                description={<span>You haven't upload any report yet</span>}
              >
                <a href="\#form-container">
                  <Button type="primary">Add Report</Button>
                </a>
              </Empty>
            )}
          </div>
        </div>
      </div>
    </main>
  );
}

const listItems = (reportData: Report[]) => (
  <Collapse
    bordered={false}
    defaultActiveKey={["1"]}
    style={{ background: "transparent" }}
    expandIcon={({ isActive }) => (
      <CaretRightOutlined rotate={isActive ? 90 : 0} />
    )}
    className="site-collapse-custom-collapse"
  >
    {reportData.map((data, index) => (
      <CollapsePanel
        header={
          <div className="report-topic">
            <span className="dot"></span>
            <span>{data.topic}</span>
          </div>
        }
        key={index}
        className="card report-card"
      >
        <div className="report-detail">
          <span>{data.detail}</span>
        </div>
      </CollapsePanel>
    ))}
  </Collapse>
);
