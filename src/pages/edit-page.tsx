import React from "react";
import { useMoralis } from "react-moralis";
import NotConnected from "../components/notConnected";
import "../styles/profile-edit.css";
import { useForm } from "react-hook-form";
import { Button, Typography, Input, Select, Avatar } from "antd";
import { FormData } from "../components/header/createProfile";
import { useAppDispatch, useAppSelector } from "../hooks/store";
import { authUser, User } from "../hooks/reducer";
import notification from "../components/notification";
import codes from "../assets/codes";
import { NavLink } from "react-router-dom";
import { UserAddOutlined } from "@ant-design/icons";
import { nanoid } from "@reduxjs/toolkit";
import NoProfile from "../components/noProfile";

const { Option } = Select;
const selectBefore = "https://";

export function ExtractSocialLinks(currentUser: User) {
  if (!currentUser?.social) return {};

  let socialMedias: { [x in keyof User["social"]]?: string } = {};
  Object.keys(currentUser.social).forEach((link: string) => {
    socialMedias[link as keyof User["social"]] =
      currentUser.social[link as keyof User["social"]];
  });

  return socialMedias;
}

const getBlob = (file: File): Promise<string> =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      const byteCharacters = window.atob(
        String(
          reader.result!.slice((reader.result as string).indexOf(",") + 1),
        ),
      );
      const byteNumbers = new Array(byteCharacters.length);

      for (let i = 0; i < byteCharacters.length; i++) {
        byteNumbers[i] = byteCharacters.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      const blob = new Blob([byteArray], { type: file.type });
      const filename = URL.createObjectURL(blob);
      resolve(filename);
    };
    reader.onerror = (error) => reject(error);
  });

export default function EditProfile() {
  const { isAuthenticated, user, Moralis } = useMoralis();
  const currentUser = user?.toJSON() as unknown as User;
  const [creating, setCreating] = React.useState<boolean>(false);
  const InputRef = React.useRef<HTMLInputElement>(null);
  const activeUser = useAppSelector((state) => state.web3Talk.user);
  const [fileList, setFileList] = React.useState<string | null>(null);
  const [prefix, setPrefix] = React.useState<string>("+44");
  const dispatch = useAppDispatch();
  const {
    handleSubmit,
    register,
    setValue,
    formState: { errors },
    reset,
    watch,
  } = useForm<FormData>();
  const allFormData = watch();
  const {
    fullname,
    email,
    phone,
    role,
    github,
    linkedin,
    gitlab,
    location,
    instagram,
    twitter,
    password,
    gender,
    username,
    usdtWalletAddress,
  } = allFormData;

  const USER = {
    fullname: currentUser?.fullname,
    email: currentUser?.email,
    phone: currentUser?.phone,
    role: currentUser?.role,
    location: currentUser?.location,
    gender: currentUser?.gender,
    username: currentUser?.username,
    profile_image: currentUser?.profile_image,
    ...ExtractSocialLinks(currentUser),
  };

  const Submit = (data: FormData) => {
    if (creating) return;

    try {
      setCreating(true);
      const socialLinks = [
        "linkedin",
        "instagram",
        "gitlab",
        "github",
        "twitter",
      ] as const;
      type SOCIAL = typeof socialLinks[number];

      let social: Partial<Pick<FormData, SOCIAL>> = {};

      for (const key in data) {
        if (socialLinks.includes(key as SOCIAL)) {
          social[key as SOCIAL] = data[key as keyof FormData];
        } else {
          user?.set(key, data[key as keyof FormData]);
        }
      }

      user?.set("social", social);
      user?.save();

      reset(data);
      setCreating(false);

      const currentUser = user?.toJSON() as unknown as User;
      dispatch(authUser(currentUser));

      const title = "Profile Updated Successfuly";
      const description = "";

      notification("success", title, description, "topRight");
    } catch (error) {
      console.error(error);
      setCreating(false);
    }
  };

  const handleChange = (key: keyof FormData, value: string) => {
    setValue(key, value);
  };

  React.useEffect(() => {
    if (isAuthenticated) {
      reset(USER);
    }
  }, [isAuthenticated, reset]);

  if (!isAuthenticated) {
    return (
      <div className="home">
        <NotConnected />
      </div>
    );
  }

  const PhoneCodes = (
    <Select defaultValue={"+44"} onChange={(e) => setPrefix(e)}>
      {codes.map((country) => (
        <Option value={country.dialCode} key={country.name}>
          <h5>
            {country.iso} {country.dialCode}
          </h5>
        </Option>
      ))}
    </Select>
  );

  const handleFileChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
    try {
      const files = e.target.files;
      const filename = await getBlob(files![0]);
      setFileList(filename);

      const file = new Moralis.File(nanoid() + USER?.username, files![0]);
      await file.saveIPFS();

      // @ts-ignore
      user?.set("profile_image", file.ipfs());
      await user?.save();

      const title = "Profile Image Changed";
      const description = "";

      notification("success", title, description, "topRight");
    } catch (error) {
      const title = "Unable to update profile image";
      const description = "Check your internet connection";
      notification("error", title, description, "topRight");
    }
  };

  if (!activeUser?.email || !activeUser?.fullname) {
    return <NoProfile />;
  }

  return (
    <main className="main-content editing-profile">
      <div className="navigation-flow">
        <NavLink to="/home">
          <span>Home</span>
        </NavLink>
        <span className="iconify" data-icon="ep:arrow-right"></span>
        <NavLink to="/profile">
          <span>Profile</span>
        </NavLink>
        <span className="iconify" data-icon="ep:arrow-right"></span>
        <a href="#">
          <span style={{ color: "#ff7a00" }}>Edit Profile</span>
        </a>
      </div>
      <div className="form-container">
        <form
          action="#"
          style={{ marginTop: "2em" }}
          className="form"
          onSubmit={handleSubmit(Submit)}
        >
          <div className="user-image" onClick={() => InputRef.current?.click()}>
            <input
              type="file"
              name="userImage"
              id="image"
              style={{ display: "none" }}
              accept="image/*"
              onChange={handleFileChange}
              ref={InputRef}
            />
            <Avatar
              size={64}
              src={
                fileList ??
                USER.profile_image ?? (
                  <UserAddOutlined
                    style={{ fontSize: "30px", color: "grey" }}
                  />
                )
              }
            />
          </div>

          <Button
            style={{ marginBottom: "2em" }}
            onClick={() => InputRef.current?.click()}
          >
            Change
          </Button>
          <div
            className="user-details section"
            style={{ justifyContent: "initial" }}
          >
            <div className="form-group">
              <label htmlFor="fullName">
                <span className="details">
                  Full Name <b style={{ color: "red" }}>*</b>
                </span>
              </label>
              <Input
                type="text"
                id="fullName"
                placeholder="Enter your full name"
                className="form-control"
                value={fullname ?? ""}
                status={errors.fullname ? "error" : undefined}
                {...register("fullname", {
                  required: true,
                  onChange: (e) => handleChange("fullname", e.target.value),
                })}
              />
            </div>
            <div className="form-group">
              <label htmlFor="fullName">
                <span className="details">
                  Username <b style={{ color: "red" }}>*</b>
                </span>
              </label>
              <Input
                type="text"
                id="username"
                placeholder="Choose a username"
                className="form-control"
                value={username ?? ""}
                status={errors.username ? "error" : undefined}
                {...register("username", {
                  required: true,
                  onChange: (e) => handleChange("username", e.target.value),
                })}
              />
            </div>
            <div className="form-group">
              <label htmlFor="role">
                {" "}
                <span className="details">
                  Role <b style={{ color: "red" }}>*</b>
                </span>
              </label>
              <Input
                type="text"
                id="role"
                placeholder="Assigned Role"
                className="form-control"
                value={role ?? ""}
                status={errors.role ? "error" : undefined}
                {...register("role", {
                  required: true,
                  onChange: (e) => handleChange("role", e.target.value),
                })}
              />
            </div>
            <div className="form-group">
              <label htmlFor="location">
                <span className="details">
                  Current Location <b style={{ color: "red" }}>*</b>
                </span>
              </label>
              <Input
                type="text"
                id="location"
                placeholder="Ex. Lagos, Nigeria."
                className="form-control"
                value={location ?? ""}
                status={errors.location ? "error" : undefined}
                {...register("location", {
                  required: true,
                  onChange: (e) => handleChange("location", e.target.value),
                })}
              />
            </div>
            <div className="form-group">
              <label htmlFor="gender">
                <span className="details">
                  Gender <b style={{ color: "red" }}>*</b>
                </span>
              </label>
              <Select
                style={{ width: "100%" }}
                bordered={false}
                allowClear
                className="form-control select"
                value={gender ?? ""}
                placeholder={"-- select gender --"}
                aria-label="Gender"
                onChange={(value) => handleChange("gender", value)}
              >
                <Option value="male">Male</Option>
                <Option value="female">Female</Option>
              </Select>
            </div>
          </div>
          <div className="social-wrapper section">
            <div className="social-title">Social Links</div>
            <div className="social-links" style={{ justifyContent: "initial" }}>
              <div className="form-group">
                <label htmlFor="github">
                  <span className="details">GitHub Url</span>
                </label>
                <Input
                  type="text"
                  id="github"
                  placeholder="GitHub Profile"
                  className="form-control"
                  value={github ?? ""}
                  {...register("github", {
                    onChange: (e) => handleChange("github", e.target.value),
                  })}
                  addonBefore={selectBefore}
                />
              </div>
              <div className="form-group">
                <label htmlFor="gitlab">
                  <span className="details">Gitlab Url</span>
                </label>
                <Input
                  type="text"
                  id="gitlab"
                  placeholder="Gitlab Profile"
                  className="form-control"
                  value={gitlab ?? ""}
                  {...register("gitlab", {
                    onChange: (e) => handleChange("gitlab", e.target.value),
                  })}
                  addonBefore={selectBefore}
                />
              </div>
              <div className="form-group">
                <label htmlFor="gitlinked">
                  <span className="details">LinkedIn Url</span>
                </label>
                <Input
                  type="text"
                  id="gitlinked"
                  placeholder="LinkedIn Profile"
                  className="form-control"
                  {...register("linkedin", {
                    onChange: (e) => handleChange("linkedin", e.target.value),
                  })}
                  value={linkedin ?? ""}
                  addonBefore={selectBefore}
                />
              </div>
              <div className="form-group">
                <label htmlFor="twitter">
                  <span className="details">Twitter</span>
                </label>
                <Input
                  type="text"
                  id="twitter"
                  placeholder="Twitter"
                  className="form-control"
                  {...register("twitter", {
                    onChange: (e) => handleChange("twitter", e.target.value),
                  })}
                  value={twitter ?? ""}
                  addonBefore={selectBefore}
                />
              </div>
              <div className="form-group">
                <label htmlFor="instagram">
                  <span className="details">Instagram Url</span>
                </label>
                <Input
                  type="text"
                  id="instagram"
                  placeholder="instagram Profile"
                  className="form-control"
                  {...register("instagram", {
                    onChange: (e) => handleChange("instagram", e.target.value),
                  })}
                  value={instagram ?? ""}
                  addonBefore={selectBefore}
                />
              </div>
            </div>
          </div>
          <div className="login-details-wrapper">
            <div className="login-title title">Creditials</div>
            <div
              className="login-details section"
              style={{ justifyContent: "initial" }}
            >
              <div className="form-group">
                <label htmlFor="phone">
                  {" "}
                  <span className="details">
                    USDT WalletAddress <b style={{ color: "red" }}>*</b>
                  </span>
                </label>
                <Input
                  type="text"
                  id="wallet-address"
                  placeholder="USDT wallet address"
                  className="form-control"
                  addonBefore={"USDT"}
                  value={usdtWalletAddress ?? ""}
                  status={errors.usdtWalletAddress ? "error" : undefined}
                  {...register("usdtWalletAddress", {
                    required: true,
                    onChange: (e) =>
                      handleChange("usdtWalletAddress", e.target.value),
                  })}
                />
              </div>
              <div className="form-group">
                <label htmlFor="phone">
                  {" "}
                  <span className="details">
                    Phone <b style={{ color: "red" }}>*</b>
                  </span>
                </label>
                <Input
                  type="text"
                  id="phone"
                  placeholder="Enter your phone no."
                  className="form-control"
                  addonBefore={PhoneCodes}
                  prefix={prefix}
                  inputMode={"tel"}
                  value={phone ?? ""}
                  status={errors.phone ? "error" : undefined}
                  {...register("phone", {
                    required: true,
                    onChange: (e) => handleChange("phone", e.target.value),
                  })}
                />
              </div>
              <div className="form-group">
                <label htmlFor="email">
                  {" "}
                  <span className="details">
                    Email <b style={{ color: "red" }}>*</b>
                  </span>
                </label>
                <Input
                  type="text"
                  id="email"
                  placeholder="Ex: tjdibbs@gmail.com"
                  className="form-control"
                  style={{ padding: "1.1em .8em" }}
                  status={errors.email ? "error" : undefined}
                  {...register("email", {
                    required: true,
                    onChange: (e) => handleChange("email", e.target.value),
                  })}
                  value={email ?? ""}
                />
              </div>
              <div className="form-group">
                <label htmlFor="phone">
                  {" "}
                  <span className="details">Password</span>
                </label>
                <Input.Password
                  id="password"
                  placeholder="Enter a new password"
                  className="form-control"
                  style={{ padding: "1.1em .8em" }}
                  status={errors.password ? "error" : undefined}
                  {...register("password", {
                    onChange: (e) => handleChange("password", e.target.value),
                  })}
                  value={password ?? ""}
                />
              </div>
            </div>
          </div>
          <div className="save-btn">
            <Button
              type="primary"
              size="large"
              loading={creating}
              htmlType="submit"
              disabled={JSON.stringify(USER) === JSON.stringify(allFormData)}
              className="save btn action-btn"
            >
              {creating ? "Updating profile ..." : "Save Profile"}
            </Button>
          </div>
        </form>
      </div>
    </main>
  );
}
