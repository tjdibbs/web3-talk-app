import React from "react";
import {
  AudioOutlined,
  EditOutlined,
  FieldTimeOutlined,
  MonitorOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Avatar, Card, Col, Divider, Row, Skeleton, Typography } from "antd";
import Meta from "antd/lib/card/Meta";
import { useMoralis } from "react-moralis";
import { useNavigate } from "react-router-dom";
import NoProfile from "../components/noProfile";
import { useAppSelector } from "../hooks/store";
import NotConnected from "../components/notConnected";
import { format } from "date-fns";

import "../styles/index.css";

function Home() {
  const { isAuthenticated, account } = useMoralis();
  const navigate = useNavigate();
  const [loading, setLoading] = React.useState<boolean>(true);
  const activeUser = useAppSelector((state) => state.web3Talk.user);

  React.useLayoutEffect(() => {
    let timeout = setTimeout(() => setLoading(false), 2000);
    return () => clearTimeout(timeout);
  }, []);

  if (!isAuthenticated) {
    return (
      <div className="home">
        <NotConnected />
      </div>
    );
  }

  // if (!activeUser?.email || !activeUser?.fullname) {
  //   return <NoProfile />;
  // }

  return (
    <div className="home">
      {/* <Card
        style={{
          width: 300,
          marginTop: 16,
          borderRadius: "20px",
          overflow: "hidden",
          boxShadow: "var(--primary-shadow)",
        }}
        loading={loading}
        className={"user-wrapper"}
        actions={[
          <EditOutlined key="edit" onClick={() => navigate("/profile/edit")} />,
          <MonitorOutlined
            key={"monitor"}
            onClick={() => navigate("/payment")}
          />,
        ]}
      >
        <Skeleton loading={loading} avatar active>
          <Meta
            avatar={
              <Avatar src={activeUser?.profile_image ?? <UserOutlined />} />
            }
            title={activeUser.fullname}
            description={
              <span>
                Hello{" "}
                <b style={{ color: "var(--primary-color)" }}>
                  {activeUser.username}
                </b>
                , nice to see you
              </span>
            }
          />
        </Skeleton>
      </Card>
      <section className="coming-topics">
        <div className="topic-list" style={{ marginTop: "2em" }}>
          <Divider orientation="left">
            <Typography.Title level={5}>UPCOMING HOT TOPICS</Typography.Title>
          </Divider>
          <Row justify="start" gutter={[16, 16]} style={{ height: "100%" }}>
            {topics.map((topic) => {
              return (
                <Col
                  key={topic.title}
                  className="gutter-row"
                  xs={24}
                  sm={8}
                  md={6}
                >
                  <Card
                    hoverable
                    style={{
                      borderRadius: "10px",
                      overflow: "hidden",
                      height: "100%",
                    }}
                    cover={
                      <img
                        alt={topic.title}
                        style={{ height: "250px" }}
                        src={"/images/" + topic.image}
                      />
                    }
                  >
                    <Skeleton loading={loading}>
                      <Meta
                        title={topic.title}
                        description={topic.agenda}
                        className={"topic-meta"}
                      />
                      <div
                        className="speaker"
                        style={{
                          background: "#f2f2f2",
                          padding: ".5em",
                          borderRadius: "10px",
                          marginTop: "1em",
                        }}
                      >
                        <AudioOutlined />{" "}
                        <span style={{ fontWeight: 700, marginLeft: ".5em" }}>
                          {topic.speaker}
                        </span>
                      </div>
                      <div
                        className="date"
                        style={{
                          borderRadius: "10px",
                          marginTop: "1em",
                        }}
                      >
                        <FieldTimeOutlined
                          style={{ color: "var(--primary-color)" }}
                        />
                        <span
                          style={{
                            fontWeight: 600,
                            marginLeft: ".5em",
                            color: "var(--primary-color)",
                          }}
                        >
                          {format(topic.date, "dd MMM, yyyy.  hh:mm aaa")}
                        </span>
                      </div>
                    </Skeleton>
                  </Card>
                </Col>
              );
            })}
          </Row>
        </div>
      </section> */}
    </div>
  );
}

const topics = [
  {
    title: "Metaverse",
    agenda:
      "Explain why there is metaverse and the part is playing in the world contemporarily",
    image: "/metaverse.jpg",
    speaker: "Darey Olushina",
    role: "Lead Engineer",
    date: new Date("2022-08-14"),
  },
  {
    title: "Blockchain",
    agenda:
      "Talking about how the word block chain is form and it's effects now",
    image: "/blockchain.jpg",
    speaker: "Darey Olushina",
    role: "Lead Engineer",
    date: new Date("2022-08-17"),
  },
  {
    title: "Web3",
    agenda: "Talking about what form web3 and where it is going.",
    image: "/web3.jpg",
    speaker: "Darey Olushina",
    role: "Lead Engineer",
    date: new Date("2022-08-20"),
  },
  {
    title: "Javascript And Blockchain",
    agenda: "How javaScript can interact with blockChian with Web3 Apis.",
    image: "/js_and_blockchain.jpg",
    speaker: "Darey Olushina",
    role: "Lead Engineer",
    date: new Date("2022-08-23"),
  },
];

export default Home;
