import React from "react";
import { createRoot } from "react-dom/client";
import App from "./App";
import { MoralisProvider } from "react-moralis";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./hooks/store";

const container = document.getElementById("container") as HTMLDivElement;
const root = createRoot(container); // createRoot(container!) if you use TypeScript
root.render(
  <BrowserRouter>
    <MoralisProvider
      serverUrl="https://2yt6qqu9bbig.usemoralis.com:2053/server"
      appId="glu0UxawhVFkrV6z3baLGmTM6578uZKBO51jJjkl"
    >
      <Provider store={store}>
        <App />
      </Provider>
    </MoralisProvider>
  </BrowserRouter>,
);
