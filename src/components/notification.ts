import { notification } from "antd";
import { NotificationPlacement } from "antd/lib/notification";
import React from "react";

export default (
  variant: "success" | "error" | "info",
  title: string,
  description: React.ReactNode,
  placement: NotificationPlacement,
  btn?: React.ReactNode,
  key?: string,
) => {
  notification[variant]({
    message: title,
    description: description,
    placement,
    btn,
    key,
    onClick: () => {
      console.log("Notification Clicked!");
    },
  });
};

export const close = (key: string) => notification.close(key);
