import React, { SetStateAction } from "react";
import { useMoralis } from "react-moralis";
import { Modal, Spin, Typography, Space, Button } from "antd";
import { connectors } from "./config";
import { useAppDispatch } from "../../hooks/store";
import { User } from "../../hooks/reducer";
import notification, { close } from "../notification";

const styles: { [x: string]: React.CSSProperties } = {
  connector: {
    alignItems: "center",
    display: "flex",
    flexDirection: "column",
    height: "100%",
    justifyContent: "center",
    marginLeft: "auto",
    marginRight: "auto",
    padding: "20px 5px",
    cursor: "pointer",
    background: "#fff",
    borderRadius: "20px",
    width: "100%",
    boxShadow: "var(--primary-shadow)",
  },
  icon: {
    alignSelf: "center",
    fill: "rgb(40, 13, 95)",
    flexShrink: "0",
    marginBottom: "8px",
    height: "30px",
  },
};

type Props = {
  setIsAuthModalVisible: React.Dispatch<SetStateAction<boolean>>;
};

export default function ConnectWalletModal(props: Props) {
  const { authenticate, Moralis } = useMoralis();
  const { setIsAuthModalVisible } = props;
  const [connectorTitle, setConnectorTitle] = React.useState<string>("");
  const dispatch = useAppDispatch();

  const handleWalletConnect = async (connectorId: string, title: string) => {
    try {
      setConnectorTitle(title);
      // @ts-ignore
      await authenticate({ provider: connectorId });
      // if (!Moralis.User.current())
      //   throw new Error("Rejected Connection Signature");

      window.localStorage.setItem("connectorId", connectorId);
      setIsAuthModalVisible(false);
      setConnectorTitle("");
    } catch (e: any) {
      console.error(e);
      setConnectorTitle("");
      notification(
        "error",
        e.message,
        "If you think this is an internal error, please contact our support.",
        "top",
      );
    }
  };

  React.useEffect(() => {
    // @ts-ignore
    window.Moralis = Moralis;
  }, []);

  return (
    <Modal
      visible={true}
      footer={null}
      onCancel={() => setIsAuthModalVisible(false)}
      bodyStyle={{
        padding: "15px",
        fontSize: "17px",
        fontWeight: "500",
      }}
      style={{
        fontSize: "16px",
        fontWeight: "500",
        borderRadius: "20px",
        overflow: "hidden",
      }}
      wrapClassName="wallet-connector"
      width="340px"
    >
      <Typography.Title
        level={5}
        style={{ textAlign: "center", marginBottom: "1em" }}
      >
        Connect Wallet
      </Typography.Title>
      <div
        style={{
          display: "grid",
          gap: "1em 2em",
          paddingInline: "1em",
          gridTemplateColumns: "1fr 1fr",
        }}
      >
        {connectors.map(({ title, icon, connectorId }, key) => (
          <div
            style={styles.connector}
            key={key}
            onClick={() => handleWalletConnect(connectorId, title)}
          >
            {title === connectorTitle ? (
              <Spin />
            ) : (
              <img src={icon} alt={title} style={styles.icon} />
            )}
            <Typography.Text style={{ fontSize: "14px" }}>
              {title}
            </Typography.Text>
          </div>
        ))}
      </div>
    </Modal>
  );
}
