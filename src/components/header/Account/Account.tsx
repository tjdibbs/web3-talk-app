import { useMoralis, useNativeBalance } from "react-moralis";
import React from "react";
import { getEllipsisTxt } from "../../../helpers/formatters";
import { Button, Card, Modal, Space, message } from "antd";
import { useState } from "react";
import { SelectOutlined } from "@ant-design/icons";
import { getExplorer } from "../../../helpers/networks";
import ConnectWalletModal from "../ConnectWalletModal";
import { NavLink } from "react-router-dom";
import Blockies from "react-blockies";
import Chains from "../Chains";
import { useAppSelector } from "../../../hooks/store";
import { Box, Chip, Typography, Stack } from "@mui/material";

export default function Account() {
  const { isAuthenticated, account, chainId, logout, user } = useMoralis();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const { data: balance } = useNativeBalance();
  const [isAuthModalVisible, setIsAuthModalVisible] = useState(false);
  const activeUser = useAppSelector((state) => state.web3Talk.user);

  const Copy = () => {
    navigator.clipboard.writeText((account as string) ?? "");
    message.info("Copied to Clipboard!", 10);
  };

  if (!isAuthenticated || !account) {
    return (
      <>
        <div onClick={() => setIsAuthModalVisible(true)}>
          <Button
            style={{
              borderColor: "var(--primary-color)",
              borderRadius: "20px",
              color: "var(--primary-color)",
            }}
          >
            Connect Wallet
          </Button>
        </div>
        {isAuthModalVisible && (
          <ConnectWalletModal setIsAuthModalVisible={setIsAuthModalVisible} />
        )}
      </>
    );
  }

  return (
    <>
      <Space direction="horizontal">
        <div
          className="account preview"
          onClick={() => setIsModalVisible(true)}
        >
          <Typography
            className="active-address"
            sx={{
              mr: "5px",
              fontWeight: 600,
              color: "var(--primary-color)",
            }}
          >
            {getEllipsisTxt(user?.get("ethAddress"), 5)}
          </Typography>
          <Blockies
            seed={account.toLowerCase()}
            className="user-image"
            scale={3}
          />
        </div>
        <Chip
          label={
            <Typography
              variant={"caption"}
              fontWeight={700}
              fontFamily={"Nunito"}
              color={"var(--primary-color)"}
            >
              {balance.formatted}
            </Typography>
          }
        />
      </Space>
      <Modal
        visible={isModalVisible}
        footer={null}
        className={"menu-modal"}
        onCancel={() => setIsModalVisible(false)}
        bodyStyle={{
          padding: "15px",
          fontSize: "17px",
          fontWeight: "500",
          borderRadius: "20px",
        }}
        style={{ fontSize: "16px", fontWeight: "500" }}
        width="300px"
      >
        Account
        <Card
          style={{
            marginTop: "10px",
            borderRadius: "1rem",
          }}
          bodyStyle={{ padding: "15px" }}
        >
          <Stack justifyContent={"center"} alignItems="center" spacing={2}>
            <Blockies
              seed={account.toLowerCase()}
              className="user-image"
              scale={6}
            />
            <Chip
              size={"small"}
              label={user?.get("ethAddress")}
              onClick={Copy}
            />
            <Typography
              variant={"subtitle1"}
              fontWeight={600}
              fontFamily={"Nunito"}
              color={"var(--primary-color)"}
            >
              {balance.formatted}
            </Typography>
            {/* <div className="user-email">{activeUser?.email}</div> */}
            <Chains />
            <div style={{ marginTop: "10px", padding: "0 10px" }}>
              <a
                href={`${getExplorer(chainId)}/address/${account}`}
                target="_blank"
                rel="noreferrer"
              >
                <SelectOutlined style={{ marginRight: "5px" }} />
                View on Explorer
              </a>
            </div>
            <hr />
          </Stack>
          <Box display={{ sm: "none" }} className="nav-link">
            <ul className="link-list">
              {links.map((link) => (
                <li key={link.label} className="link listitem active">
                  <NavLink
                    to={link.to}
                    onClick={() => setIsModalVisible(false)}
                    className={({ isActive }) =>
                      isActive ? "active" : undefined
                    }
                  >
                    {link.label}
                  </NavLink>
                </li>
              ))}
            </ul>
          </Box>
        </Card>
        <Button
          size="large"
          type="primary"
          style={{
            width: "100%",
            marginTop: "10px",
            borderRadius: "0.5rem",
            borderColor: "var(--primary-color)",
            fontSize: "16px",
            fontWeight: "500",
            background: "var(--primary-color)",
          }}
          onClick={async () => {
            await logout();
            window.localStorage.removeItem("connectorId");
            setIsModalVisible(false);
          }}
        >
          Disconnect Wallet
        </Button>
      </Modal>
    </>
  );
}

const links = [
  {
    label: "Report",
    to: "/report",
  },
  {
    label: "Profile",
    to: "/profile",
  },
  {
    label: "Payment",
    to: "/payment",
  },
];
