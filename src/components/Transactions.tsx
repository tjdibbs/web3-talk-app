import { Chip, Divider, Grid, Stack, Typography } from "@mui/material";
import { Button, Card, Empty, message } from "antd";
import React from "react";
import { useMoralis, useMoralisWeb3Api } from "react-moralis";

type Transaction = {
  hash: string;
  to_address: string;
  value: string;
  block_timestamp: string;
};

export default function Transactions() {
  const Web3Api = useMoralisWeb3Api();
  const { account } = useMoralis();
  const [transactions, setTransactions] = React.useState<Transaction[]>([]);

  const fetchTransactions = async function* () {
    // get mainnet transactions for the current user
    yield await Web3Api.account.getTransactions({ address: account as string });
  };

  React.useEffect(() => {
    (async (t) => {
      for await (const transaction of t()) {
        setTransactions(transaction.result as Transaction[]);
      }
    })(fetchTransactions);
  }, []);

  const Copy = (text: string) => {
    navigator.clipboard.writeText(text);
    message.info("Copied to clipboard", 5);
  };

  return (
    <div className={"payment-portal"}>
      <div className="main-title">
        <Divider textAlign={"center"}>
          <Typography
            variant={"h6"}
            color={"primary"}
            fontWeight={700}
            fontFamily={"Nunito"}
          >
            Transanction Histories
          </Typography>
        </Divider>
      </div>
      <div className="active-payment">
        <Grid container>
          {transactions?.map((t, index) => (
            <Grid item xs={12} sm={4} key={index}>
              <Card style={{ width: 300, marginTop: 16 }}>
                <Stack spacing={2}>
                  <Typography>
                    <span>Hash:</span>{" "}
                    <Chip label={t.hash} onClick={() => Copy(t.hash)} />
                  </Typography>
                  <Typography>
                    <span>Receiver Address:</span>{" "}
                    <Chip
                      label={t.to_address}
                      onClick={() => Copy(t.to_address)}
                    />
                  </Typography>
                  <Typography>
                    <span>Amount: </span>{" "}
                    <Chip label={t.value} onClick={() => Copy(t.value)} />
                  </Typography>
                  <Typography>
                    <span>Date: </span>{" "}
                    <Chip
                      label={new Date(t.block_timestamp).toLocaleString()}
                    />
                  </Typography>
                </Stack>
              </Card>
            </Grid>
          ))}
        </Grid>
      </div>
      {transactions.length === 0 && (
        <Empty
          image="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"
          imageStyle={{
            height: 60,
          }}
          description={
            <span>
              Couldn't Fetch Any mainnet transactions for the current address
            </span>
          }
        ></Empty>
      )}
    </div>
  );
}
