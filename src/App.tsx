import React from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import "antd/dist/antd.min.css";

// Pages
// import Homepg from "./pages/Home";
import Reportpg from "./pages/Report";
import Profilepg from "./pages/Profile";
import MakePayment from "./pages/MakePayment";
import Header from "./components/header";
import EditProfile from "./pages/edit-page";
import { useMoralis } from "react-moralis";
import { useAppDispatch } from "./hooks/store";
import { authUser, User } from "./hooks/reducer";
import ErrorPage from "./pages/ErrorPage";

const App = () => {
  const { Moralis, user } = useMoralis();
  const dispatch = useAppDispatch();

  React.useLayoutEffect(() => {
    const currentUser = user?.toJSON() as unknown as User;
    dispatch(authUser(currentUser));
  }, [user]);

  return (
    <React.Fragment>
      <Header />
      {/* The Switch decides which component to show based on the current URL.*/}
      <div className="wrapper">
        <Routes>
          <Route path="/" element={<Navigate to={"/payment"} />} />
          <Route path="/report" element={<Reportpg />} />
          <Route path="/profile" element={<Profilepg />} />
          <Route path="/profile/edit" element={<EditProfile />} />
          <Route path="/payment" element={<MakePayment />} />
          <Route path="*" element={<ErrorPage />} />
        </Routes>
      </div>
    </React.Fragment>
  );
};

export default App;
