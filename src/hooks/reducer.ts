import { createSlice, nanoid, PayloadAction } from "@reduxjs/toolkit";
// import cookie from "js-cookie";

export type User = {
  objectId: string;
  fullname: string;
  phone: string;
  role: string;
  location: string;
  gender: string;
  profile_image: string;
  email: string;
  username: string;
  addresses: string[];
  usdtWalletAddress: string;
  social: {
    instagram: string;
    gitlab: string;
    github: string;
    twitter: string;
    linkedin: string;
  };
};

export interface State {
  user: User | null;
}

const initialState: State = {
  user: null,
};

const AppContext = createSlice({
  name: "web3",
  initialState,
  reducers: {
    authUser: (state: State, actions: PayloadAction<User>) => {
      state.user = actions.payload;
    },
  },
});

export const { authUser } = AppContext.actions;
export default AppContext.reducer;
