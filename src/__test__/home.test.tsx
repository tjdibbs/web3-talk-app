import React from "react";
import { render, cleanup } from "@testing-library/react";
import { act } from "react-dom/test-utils";
// import { render, unmountComponentAtNode } from "react-dom";
// import pretty from "pretty";s
import { MoralisProvider } from "react-moralis";
import { Provider } from "react-redux";
import store from "../hooks/store";
import Status from "../pages/Home";

afterEach(cleanup);

it("should render a greeting", () => {
  let { container, asFragment } = render(
    <MoralisProvider
      serverUrl="https://2yt6qqu9bbig.usemoralis.com:2053/server"
      appId="glu0UxawhVFkrV6z3baLGmTM6578uZKBO51jJjkl"
    >
      <Provider store={store}>
        <Status />
      </Provider>
    </MoralisProvider>,
  );

  expect(container).toMatchSnapshot();
});
